# -*- coding: utf-8 -*-

"""
Prepare tvb Python package for setup.
"""

import shutil
from setuptools import setup, find_packages

requirements = (
    'numpy',
    'scipy',
    'scikit-learn',
    'matplotlib',
    'trimesh',
    'anytree',
    'Pegasus',
    'h5py',
    'pytest',
    'Cython',
    'gdist',
    'nibabel==3.2.1'
)

setup(
    name="tvb-recon",
    description="Brain Network Models - Reconstruction tool from structural MR scans",
    packages=find_packages(),
    version="0.2",
    license="GPL",
    author="BNM Team",
    install_requires=requirements,
)

shutil.rmtree('tvb.egg-info', True)
